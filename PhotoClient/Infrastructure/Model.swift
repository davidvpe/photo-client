//
//  Model.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

class ImageModel {
    let id: Int
    let previewURL: String
    let imageURL: String
    var previewImage: UIImage? = nil
    var fullImage: UIImage? = nil

    init(id: Int, previewURL: String, imageURL: String, previewImage: UIImage? = nil, fullImage: UIImage? = nil) {
        self.id = id
        self.previewURL = previewURL
        self.imageURL = imageURL
        self.previewImage = previewImage
        self.fullImage = fullImage
    }
}
