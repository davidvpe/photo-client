
//
//  LibraryViewController.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//

import UIKit

protocol LibraryDisplayLogic: class {
    func loadImages(viewModel: Library.Load.ViewModel)
    func handleError(description: String)
}

class LibraryViewController: UIViewController, LibraryDisplayLogic {

    var interactor: LibraryBusinessLogic?
    var router: (NSObjectProtocol & LibraryRoutingLogic & LibraryDataPassing)?

    var photos = [ImageModel]()

    var customInteractor: TransitionInteractor?

    var isFetchingMorePhotos: Bool = false
    var currentPage: Int = 0
    var hasReachedTheEnd: Bool = false

    var libraryView: LibraryView {
        guard let view = view as? LibraryView else {
            return LibraryView()
        }
        return view
    }

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func loadView() {
        view = LibraryView()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        let interactor = LibraryInteractor()
        let presenter = LibraryPresenter()
        let router = LibraryRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.delegate = self

        libraryView.collectionView.dataSource = self
        libraryView.collectionView.delegate = self

        libraryView.collectionView.register(LibraryPhotoCell.self, forCellWithReuseIdentifier: CellIdentifiers.libraryPhotoCell)

        self.title = "Library"

        loadPhotos()
    }

    func handleError(description: String) {
        hasReachedTheEnd = true
        isFetchingMorePhotos = false
        print(description)
    }

    func loadPhotos() {
        currentPage = currentPage + 1
        let request = Library.Load.Request(page: currentPage)
        interactor?.firstLoad(request: request)
        isFetchingMorePhotos = true
    }

    func loadImages(viewModel: Library.Load.ViewModel) {
        var arrayIndexesToAdd = [IndexPath]()
        let lastCount = photos.count
        photos.append(contentsOf: viewModel.photos)
        for i in 0..<viewModel.photos.count {
            let indexPath = IndexPath(item: i + lastCount, section: 0)
            arrayIndexesToAdd.append(indexPath)
        }
        libraryView.collectionView.insertItems(at: arrayIndexesToAdd)
        isFetchingMorePhotos = false
    }
}

extension LibraryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.libraryPhotoCell, for: indexPath) as? LibraryPhotoCell {
            let photo = photos[indexPath.item]
            cell.setupView(withModel: photo)
            return cell
        }

        return UICollectionViewCell()
    }
}

extension LibraryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return LibraryFlowLayout.finalSize(forParentSize: UIScreen.main.bounds.size)
    }
}

extension LibraryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let attr: UICollectionViewLayoutAttributes! = collectionView.layoutAttributesForItem(at: indexPath)
        let selectedPictureFrame = collectionView.convert(attr.frame, to: collectionView.superview)

        router?.dataStore?.imageFrame = selectedPictureFrame
        router?.dataStore?.hasReachedEnd = hasReachedTheEnd
        router?.dataStore?.indexToShow = indexPath.item
        router?.dataStore?.page = currentPage
        router?.dataStore?.photos = photos

        router?.routeToPhotoViewController()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !isFetchingMorePhotos && !hasReachedTheEnd && (indexPath.item == photos.count - 1) {
            loadPhotos()
        }
    }
}

extension LibraryViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        guard let pictureIndex = router?.dataStore?.indexToShow else { return nil }
        guard let photosFromRouter = router?.dataStore?.photos else { return nil }
        photos = photosFromRouter
        let picture = photosFromRouter[pictureIndex]

        var originFrame: CGRect

        if let frame = router?.dataStore?.imageFrame {
            originFrame = frame
        } else {

            let indexPath = IndexPath(item: pictureIndex, section: 0)
            libraryView.collectionView.reloadData()
            if libraryView.collectionView.indexPathsForVisibleItems.index(of: indexPath) == nil {
                libraryView.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
            }
            let attr: UICollectionViewLayoutAttributes! = libraryView.collectionView.layoutAttributesForItem(at: indexPath)
            let selectedPictureFrame = libraryView.collectionView.convert(attr.frame, to: libraryView.collectionView.superview)

            originFrame = selectedPictureFrame
        }

        switch operation {
        case .push:
            self.customInteractor = TransitionInteractor(attachTo: toVC)
            return TransitionAnimator(duration: TimeInterval(UINavigationControllerHideShowBarDuration), isPresenting: true, originFrame: originFrame, image: picture.previewImage!)
        default:
            return TransitionAnimator(duration: TimeInterval(UINavigationControllerHideShowBarDuration), isPresenting: false, originFrame: originFrame, image: picture.previewImage!)
        }

    }

    func navigationController(_ navigationController: UINavigationController,
                              interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        guard let ci = customInteractor else { return nil }
        return ci.transitionInProgress ? customInteractor : nil
    }
}
