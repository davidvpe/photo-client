//
//  TransitionAnimator.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

class TransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    public let CustomAnimatorTag = 99

    var duration : TimeInterval
    var isPresenting : Bool
    var originFrame : CGRect
    var image : UIImage

    init(duration : TimeInterval, isPresenting : Bool, originFrame : CGRect, image : UIImage) {
        self.duration = duration
        self.isPresenting = isPresenting
        self.originFrame = originFrame
        self.image = image
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView

        guard let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
        guard let toView = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
        guard let toViewController = transitionContext.viewController(forKey: .to) else { return }
        let toViewTopBarFrame = toViewController.navigationController?.navigationBar.frame ?? CGRect.zero

        self.isPresenting ? container.addSubview(toView) : container.insertSubview(toView, belowSubview: fromView)

        let detailView = isPresenting ? toView : fromView

        var futureImagePlaceholderFrame = toView.frame
        let topMargin = toViewTopBarFrame.size.height + toViewTopBarFrame.origin.y
        futureImagePlaceholderFrame.size.height -= topMargin
        futureImagePlaceholderFrame.origin.y = topMargin

        let transitionImageView = PBImageView(image: image)
        transitionImageView.clipsToBounds = true
        transitionImageView.frame = isPresenting ? originFrame : futureImagePlaceholderFrame
        transitionImageView.contentMode = isPresenting ? .scaleAspectFill : .scaleAspectFit
        transitionImageView.image = image

        container.addSubview(transitionImageView)

        toView.alpha = isPresenting ? 0 : 1
        toView.layoutIfNeeded()

        if isPresenting {
            UIView.animate(withDuration: duration, animations: {
                transitionImageView.frame = futureImagePlaceholderFrame
                transitionImageView.contentMode = .scaleAspectFit
            }, completion: { (finished) in
                UIView.animate(withDuration: self.duration/2, animations: {
                    detailView.alpha = 1
                }, completion: { (finished) in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                    transitionImageView.removeFromSuperview()
                })
            })
        } else {
            UIView.animate(withDuration: duration/2, animations: {
                detailView.alpha = 0
            }, completion: { (finished) in
                UIView.animate(withDuration: self.duration, animations: {
                    transitionImageView.frame = self.originFrame
                    transitionImageView.contentMode = .scaleAspectFill
                }, completion: { (finished) in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                    transitionImageView.removeFromSuperview()
                })

            })
        }
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    
}
