//
//  Store.swift
//  PhotoStore
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

public enum ErrorValue<T> {
    case error(description: String)
    case value(value: T)
}

public class Store {

    let requestQueue = OperationQueue()

    public func enqueueOperationWithDependancies(operation: Operation) {
        for dependancy in operation.dependencies {
            enqueueOperationWithDependancies(operation: dependancy)
        }
        requestQueue.addOperation(operation)
    }
}

public final class AssetsStore: Store {
    public static let shared = AssetsStore()

    public func downloadImage(withURL url: String, completion: @escaping (UIImage?) -> Void) -> ImageOperation {

        let operation = ImageOperation(url) { (image) in
            completion(image)
        }

        enqueueOperationWithDependancies(operation: operation)

        return operation
    }

}


public final class PixabayStore: Store {

    let baseURL: String = "https://pixabay.com/api/"
    let apiKey = "9152527-4f4116803b3ec10f1b627c8cc"

	public static let shared = PixabayStore()
}
