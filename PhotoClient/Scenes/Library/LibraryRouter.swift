//
//  LibraryRouter.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//

import UIKit

@objc protocol LibraryRoutingLogic {
    func routeToPhotoViewController()
}

protocol LibraryDataPassing {
    var dataStore: LibraryDataStore? { get set }
}

class LibraryRouter: NSObject, LibraryRoutingLogic, LibraryDataPassing {
    weak var viewController: LibraryViewController?
    var dataStore: LibraryDataStore?

    //MARK: Routing

    func routeToPhotoViewController() {
        let destinationVC = PhotoViewController()
        var destinationDS = destinationVC.router!.dataStore!
        passingDataToPhoto(source: dataStore!, destination: &destinationDS)
        navigatingToPhoto(source: viewController!, destination: destinationVC)
    }

    // MARK: Navigation

    func navigatingToPhoto(source: LibraryViewController, destination: PhotoViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }

    // MARK: Passing data

    func passingDataToPhoto(source: LibraryDataStore, destination: inout PhotoDataStore) {
        destination.hasReachedEnd = source.hasReachedEnd
        destination.indexToShow = source.indexToShow
        destination.page = source.page
        destination.photos = source.photos
    }
}
