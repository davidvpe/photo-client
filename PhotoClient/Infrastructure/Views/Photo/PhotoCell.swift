//
//  PhotoCell.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 01/06/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit
import PhotoStore

class PhotoCell: UICollectionViewCell {

    let imageView: UIImageView
    var operation: ImageOperation?

    override init(frame: CGRect) {
        imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit

        super.init(frame: frame)

        addSubviewForAutolayout(imageView)
        setupConstraints()
    }

    override func prepareForReuse() {
        operation?.cancel()
        imageView.image = nil
    }

    func setupView(withModel model: ImageModel) {
        if let previewImage = model.previewImage {
            self.imageView.alpha = 0
            self.imageView.image = previewImage
            UIView.animate(withDuration: 0.1, animations: {
                self.imageView.alpha = 1
            })
        }

        operation = AssetsStore.shared.downloadImage(withURL: model.imageURL, completion: { (image) in
            DispatchQueue.main.async {
                self.imageView.alpha = 0
                self.imageView.image = image
                model.previewImage = image
                UIView.animate(withDuration: 0.1, animations: {
                    self.imageView.alpha = 1
                })
            }
        })

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([imageView.topAnchor.constraint(equalTo: topAnchor),
                                     imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
                                     imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
                                     imageView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
}
