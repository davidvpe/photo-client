//
//  WeatherRequest.swift
//  PhotoStore
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import Foundation

public extension PixabayStore {

    public func getImagesByPage(page: Int = 1, completionBlock: @escaping (ErrorValue<PixabayModel.HitResponse>) -> Void) -> NetworkOperation {
		let url = PixabayStore.shared.baseURL

        let params = [
			Parameter("key", apiKey),
            Parameter("q", "amsterdam"),
            Parameter("image_type", "photo"),
            Parameter("per_page", 50),
			Parameter("page", page)
		]

		let operation = NetworkOperation(url, params: params, finishedBlock: { result in
			switch result {
			case .error(let description):
				completionBlock(.error(description: description))
			case .data(let data):
				if let data = data {
                    do {
                        let response = try JSONDecoder().decode(PixabayModel.HitResponse.self, from: data)
                        completionBlock(.value(value: response))
                    } catch(let error) {
                        completionBlock(.error(description: error.localizedDescription))
                    }
				}
			}
		})
		
		return operation
	}
}
