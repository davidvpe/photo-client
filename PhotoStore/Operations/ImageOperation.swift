//
//  NetworkOperation.swift
//  PhotoStore
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

public class ImageOperation: ConcurrentOperation {
	
	var task: URLSessionDataTask? = nil
	
	var urlString: URLRepresentable
	var httpMethod: HTTPMethod
	var encoding: EncodingType
	var finishedBlock: ((UIImage?) -> Void)

	init(_ urlString: URLRepresentable, httpMethod: HTTPMethod = .get, encoding: EncodingType = .url, finishedBlock: @escaping (UIImage?) -> Void) {
		
		self.urlString = urlString
		self.httpMethod = httpMethod
		self.encoding = encoding
		self.finishedBlock = finishedBlock
	
		task = nil
		
	}
	
	override public func start() {
		
		if !isCancelled && isReady {
			
			super.start()
			
            let request = DavidFire.shared.downloadImage(urlString) { result in
				self.finish()
				self.finishedBlock(result)
			}
			
			task = request
			
			self.task!.resume()
			
		}
	}
	
	override public func cancel() {
		if self.task != nil {
			self.task!.cancel()
		}
		super.cancel()
	}
}
