//
//  LibraryWorker.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//
import Foundation
import PhotoStore

class LibraryWorker {
    func fetchPhotos(byPage page: Int, completion: @escaping (PixabayModel.HitResponse) -> Void, failure: @escaping (String) -> Void) {
        let operation = PixabayStore.shared.getImagesByPage(page: page) { result in
            switch result {
            case .value(let response):
                completion(response)
            case .error(let description):
                failure(description)
            }
        }

        PixabayStore.shared.enqueueOperationWithDependancies(operation: operation)
    }
}
