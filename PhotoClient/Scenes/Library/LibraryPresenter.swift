//
//  LibraryPresenter.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//

import UIKit

protocol LibraryPresentationLogic {
    func presentPhotos(response: Library.Load.Response)
    func presentError()
}

class LibraryPresenter: LibraryPresentationLogic {
    weak var viewController: LibraryDisplayLogic?

    // MARK: Do something
    func presentError() {
        //
    }

    func presentPhotos(response: Library.Load.Response) {
        var arrayPhotos = [ImageModel]()
        for hit in response.response.hits {
            let image = ImageModel(id: hit.id, previewURL: hit.previewURL, imageURL: hit.imageURL)
            arrayPhotos.append(image)
        }
        let viewModel = Library.Load.ViewModel(photos: arrayPhotos)
        viewController?.loadImages(viewModel: viewModel)
    }
}
