//
//  LibraryInteractor.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//

import UIKit

protocol LibraryBusinessLogic {
    func firstLoad(request: Library.Load.Request)
}

protocol LibraryDataStore {
    var imageFrame: CGRect? { get set }
    var indexToShow: Int! { get set }
    var photos: [ImageModel]! { get set }
    var page: Int! { get set }
    var hasReachedEnd: Bool! { get set }
}

class LibraryInteractor: LibraryBusinessLogic, LibraryDataStore {

    var presenter: LibraryPresentationLogic?
    var worker: LibraryWorker?

    var imageFrame: CGRect?
    var indexToShow: Int!
    var photos: [ImageModel]!
    var page: Int!
    var hasReachedEnd: Bool!

    func firstLoad(request: Library.Load.Request) {
        worker = LibraryWorker()
        worker?.fetchPhotos(byPage: request.page, completion: { response in
            DispatchQueue.main.async {
                let response = Library.Load.Response(response: response)
                self.presenter?.presentPhotos(response: response)
            }
        }, failure: { error in
            DispatchQueue.main.async {
                self.presenter?.presentError()
            }
        })
    }
}
