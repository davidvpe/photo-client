//
//  PhotoRouter.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol PhotoRoutingLogic {
    func routeBackToLibrary()
}

protocol PhotoDataPassing {
    var dataStore: PhotoDataStore? { get set }
}

class PhotoRouter: NSObject, PhotoRoutingLogic, PhotoDataPassing {


    weak var viewController: PhotoViewController?
    var dataStore: PhotoDataStore?

    // MARK: Routing

    func routeBackToLibrary() {
        guard let navigationController = viewController?.navigationController else { return }
        guard let destinationVC = navigationController.viewControllers.first as? LibraryViewController else { return }
        var destinationDS = destinationVC.router!.dataStore!
        passDataBackToLibrary(source: dataStore!, destination: &destinationDS)
        navigateBackToLibrary(source: viewController!, destination: destinationVC)
    }

    // MARK: Navigation

    func navigateBackToLibrary(source: PhotoViewController, destination: LibraryViewController) {
        source.navigationController?.popViewController(animated: true)
    }

    // MARK: Passing data

    func passDataBackToLibrary(source: PhotoDataStore, destination: inout LibraryDataStore) {
        destination.hasReachedEnd = source.hasReachedEnd
        destination.indexToShow = source.indexToShow
        destination.page = source.page
        destination.photos = source.photos
        destination.imageFrame = nil
    }
}
