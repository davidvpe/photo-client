//
//  LibraryFlowLayout.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

struct LibraryFlowLayout {

    struct ViewTraits {
        static let desiredSize: CGFloat = 70.0
        static let interitemSpace: CGFloat = 2.0
    }

    static func finalSize(forParentSize parentSize: CGSize) -> CGSize {
        let width = parentSize.width

        let finalAmount = floor((width + ViewTraits.interitemSpace)/(ViewTraits.desiredSize + ViewTraits.interitemSpace))
        let finalWidth = ((width + ViewTraits.interitemSpace - (ViewTraits.interitemSpace * finalAmount)) / finalAmount)
        return CGSize(width: finalWidth, height: finalWidth)
    }
}
