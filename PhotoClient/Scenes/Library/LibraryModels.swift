//
//  LibraryModels.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright (c) 2018 David Velarde. All rights reserved.
//

import UIKit
import PhotoStore

enum Library {
    // MARK: Use cases

    enum Load {
        struct Request {
            let page: Int
        }
        struct Response {
            let response: PixabayModel.HitResponse
        }
        struct ViewModel {
            let photos: [ImageModel]
        }
    }
}
