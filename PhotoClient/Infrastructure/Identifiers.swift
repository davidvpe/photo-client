//
//  Identifiers.swift
//  PhotoClient
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import Foundation

struct CellIdentifiers {
    static let photoCell: String = "photoCell"
    static let libraryPhotoCell: String = "libraryPhotoCell"
}
