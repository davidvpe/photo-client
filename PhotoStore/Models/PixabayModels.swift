//
//  PixabayModels.swift
//  PhotoStore
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import Foundation

public struct PixabayModel {

    public struct HitResponse: Codable {
        let total: Int
        let totalHits: Int
        public let hits: [Hit]
    }


    public struct Hit: Codable {
        public let id: Int
        public let previewURL: String
        public let imageURL: String

        enum CodingKeys: String, CodingKey {
            case id
            case previewURL
            case imageURL = "largeImageURL"
        }

        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decode(Int.self, forKey: .id)
            previewURL = try values.decode(String.self, forKey: .previewURL)
            imageURL = try values.decode(String.self, forKey: .imageURL)
        }
    }

    private init() { }
}
