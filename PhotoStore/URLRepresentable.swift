//
//  URLRepresentable.swift
//  PhotoStore
//
//  Created by Velarde Robles, David on 31/05/2018.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

typealias URLRepresentable = String
